<?php

class AdminController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
    	if ($this->session->get('auth')['rol'] != 1) {
             $this->dispatcher->forward([
                'controller' => "login",
                'action' => 'index'
            ]);
            $this->flash->error("no tienes permisos suficientes para entrar en esta seccion si intentas entrar varias veces tu IP sera baneada del sistema");
            return;
        }
    }
    public function indexAction()
    {
        $materias = Materias::find();

        $this->view->setParamToView("materias", $materias);

    }

       public function createMateriasAction()
    {

          if (!$this->request->isPost()) {
                $this->dispatcher->forward([
                    'controller' => "admin",
                    'action' => 'crear-materias'
                ]);

                return;
            }

            $materia = new Materias();
            $materia->nombre = $this->request->getPost("nombre");
            $materia->profesor = $this->request->getPost("profesor");
            $materia->horario = $this->request->getPost("horario");
            $materia->estatus = 1;
            $materia->n_aula = $this->request->getPost("n_aula");
            $materia->created_at = date('Y-m-d');
           

            if (!$materia->save()) {
                foreach ($materia->getMessages() as $message) {
                    $this->flash->error($message);
                }

                $this->dispatcher->forward([
                    'controller' => "admin",
                    'action' => 'index'
                ]);

                return;
            }
                $this->flash->success("La materia". $materia->nombre ." fue creada con exito puedes editarla o borrarla en el campo de materias registradas ");
                    $this->dispatcher->forward([
                        'controller' => "admin",
                        'action' => 'index'
                    ]);

                    return;
 
    } 

      public function delete_materiaAction($id)
    {
            if (!$this->request->isPost()) {

            $materia = Materias::findFirstByid($id);
             if (!$materia) {
                $this->flash->error("La materia no se encuentra");

                $this->dispatcher->forward([
                    'controller' => "admin",
                    'action' => 'index'
                ]);

                return;
            }

            $calificaciones = Calificaciones::findByMateriaId($id);
            foreach ($calificaciones as $calificacion) {
                $calificacion->delete();
            }
          
            $materia->delete();
              $this->flash->success("materia se borro con exito");

                    $this->dispatcher->forward([
                        'controller' => "admin",
                        'action' => 'index'
                    ]);
        }
       

    } 
          public function estudiantesAction()
    {
           
        $estudiantes = Estudiantes::find();

        $this->view->setParamToView("estudiantes", $estudiantes);

        $materias = Materias::find();

        $this->view->setParamToView("materias", $materias);

    } 
           public function createEstudiantesAction()
    {

          if (!$this->request->isPost()) {
                $this->dispatcher->forward([
                    'controller' => "admin",
                    'action' => 'estudiantes'
                ]);

                return;
            }

            $estudiante = new Estudiantes();
            $estudiante->nombre = $this->request->getPost("nombre");
            $estudiante->apellido = $this->request->getPost("apellido");
            $estudiante->f_nacimiento = $this->request->getPost("f_nacimiento");
            $estudiante->created_at = date('Y-m-d');
           

            if (!$estudiante->save()) {
                foreach ($estudiante->getMessages() as $message) {
                    $this->flash->error($message);
                }

                $this->dispatcher->forward([
                    'controller' => "admin",
                    'action' => 'estudiantes'
                ]);

                return;
            }
                $this->flash->success("El estudiante". $estudiante->nombre ." ".$estudiante->apellido . " se creo con exito puedes borrarla en el campo de estudiantes registrados ");
                    $this->dispatcher->forward([
                        'controller' => "admin",
                        'action' => 'estudiantes'
                    ]);

                    return;
 
    }
          public function delete_estudianteAction($id)
    {
            if (!$this->request->isPost()) {

            $estudiante = Estudiantes::findFirstByid($id);
             if (!$estudiante) {
                $this->flash->error("El estudiante no se encuentra");

                $this->dispatcher->forward([
                    'controller' => "admin",
                    'action' => 'estudiantes'
                ]);

                return;
            }

            $calificaciones = Calificaciones::findByEstudianteId($id);
            foreach ($calificaciones as $calificacion) {
                $calificacion->delete();
            }
          
            $estudiante->delete();
              $this->flash->success("El estudiante se borro con exito");

                    $this->dispatcher->forward([
                        'controller' => "admin",
                        'action' => 'estudiantes'
                    ]);
        }
       

    }
    public function AsignarMateriasAction(){
            $this->view->disable(); 
            if (!$this->request->isPost()) {
            $this->flash->error('Error de sistema consulte con el administrador');
            return;
        }

        $id = $this->request->getPost("id");

        $estudiante = Estudiantes::findFirst($id);
         if (!$estudiante) {
            $this->flash->error('El estudiante  No existe');
            return;
        }

         $materias = Materias::find(); 
           echo '<input type="hidden" name="estudiante_id" value="' . $id . '">';

           foreach ($materias as $materia) { 

            echo '<div class="col-sm-6"><label><input type="checkbox" name="materias[]" value="'. $materia->id .'">'. $materia->nombre. ' - '.$materia->horario.' - '. $materia->n_aula . '</label><br></div>';
             } 

            if (count($materias) < 1) {
               echo '<div class="sin-contenido"> No hay materias Cargadas </div>';
            }



    }


     public function verMateriasAction(){
         $this->view->disable(); 
            if (!$this->request->isPost()) {
            $this->flash->error('Error de sistema consulte con el administrador');
            return;
        }

        $id = $this->request->getPost("id");

        $estudiante = Estudiantes::findFirst($id);
         if (!$estudiante) {
            $this->flash->error('El estudiante  No existe');
            return;
        }

         $calificaciones = Calificaciones::findByestudianteId($id); 
          $i = 1;
           foreach ($calificaciones as $calificacion) {
             $materia = MAterias::findFirstByid($calificacion->materia_id); 
             ?>
               
                <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $materia->nombre; ?></td>
                    <td><?php echo $materia->horario; ?></td>
                    <td><?php echo $materia->profesor; ?></td>
                    <td><?php echo $materia->n_aula; ?></td>
                    <td><?php echo $calificacion->nota; ?></td>
                  

                </tr>


           
           <?php  } 

            if (count($calificaciones) < 1) {
               echo '<div class="sin-contenido"> No hay Materias asignadas  </div>';
            }



    }


    public function asignarMateriasEstudiantesAction()
    {

          if (!$this->request->isPost()) {
                $this->dispatcher->forward([
                    'controller' => "admin",
                    'action' => 'estudiantes'
                ]);

                return;
            }
               $materias = $this->request->getPost("materias");
 
               for($i=0; $i<count($materias); $i++){

                $calificaciones = new calificaciones();
                $calificaciones->materia_id = $materias[$i];
                $calificaciones->estudiante_id = $this->request->getPost("estudiante_id");
                $calificaciones->nota = "sin calificar";
                $calificaciones->created_at = date('Y-m-d');
         
        
                if (!$calificaciones->save()) {
                foreach ($calificaciones->getMessages() as $message) {
                $this->flash->error($message);
                }
                return;
            }
        }

        echo '<div class="alert alert-success" role="alert">LAs materias fueron asignadas con exito!</div>';


    return;
 
    }




}

