<?php

class LoginController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
	    {
	      $this->persistent->parameters = null;
	    }
    public function createAction()
	    {
	        if (!$this->request->isPost()) {
	            $this->dispatcher->forward([
	                'controller' => "login",
	                'action' => 'index'
	            ]);

	            return;
	        }

	        if ($this->request->getPost("clave") != $this->request->getPost("clave2")) {
	        	$this->flash->error("La contraseña no conside por favor vuelva a intentarlo");
	            $this->dispatcher->forward([
	                'controller' => "login",
	                'action' => 'index'
	            ]);

	            return;
	        }
	        $user = Usuarios::findFirstByCorreo($this->request->getPost("correo"));
	        if ($user) {
	        	$this->flash->error("El correo que intentas registrar ya se encuentra registrado");
	            $this->dispatcher->forward([
	                'controller' => "login",
	                'action' => 'index'
	            ]);

	            return;
	        }

	        $pass = $this->request->getPost("clave");
    		$usuario = new Usuarios();
			$usuario->nombre = $this->request->getPost("nombre");
			$usuario->correo = $this->request->getPost("correo");
			$usuario->telefono = $this->request->getPost("telefono");
			$usuario->clave = $this->security->hash($pass);
			$usuario->created_at = date('Y-m-d');
			$usuario->rol = 1;
	        

	        if (!$usuario->save()) {
	            foreach ($usuario->getMessages() as $message) {
	                $this->flash->error($message);
	            }

	            $this->dispatcher->forward([
	                'controller' => "login",
	                'action' => 'index'
	            ]);

	            return;
	        }

				
				return;
				$this->view->disable(); 
	    }
/*
  +------------------------------------------------------------------------+
  | los roles de usuario estan plasmados de la siguiente forma             |
  |    *rol 1 = Usuario administracion									   |
  |    *rol 2 = profesores												   |
  |    *rol 3 = Estudiantes												   |
  |    *rol 666 = Administrador general 							       |
  |																	       |
  +------------------------------------------------------------------------+
  | Authors: juan nava <juannava1230@gmail.com>                            |
  +------------------------------------------------------------------------+
*/

	      public function entrarAction()
	    {
	   

	        if (!$this->request->isPost()) {
	            $this->dispatcher->forward([
	                'controller' => "login",
	                'action' => 'index'
	            ]);

	            return;
	        }
	        $login    = $this->request->getPost("email");
	        $pass     = $this->request->getPost("pass");
	        $user = Usuarios::findFirstByCorreo($login);
	      
	        if ($user) {
	            if ($this->security->checkToken() && $this->security->checkHash($pass, $user->clave)) {

			        	// rol de usuario para entrar como admin normal
		               	if ($user->rol == 1) {
		               		 $this->session->set('auth', 
				               	 	[
			                        'id' => $user->id,
			                        'rol' => $user->rol,
			                        'nombre' => $user->nombre,
			                        'telefono' => $user->telefono,
			                        ]
		                        );
								$this->response->redirect("/admin"); 
					       	}

		           	}else {
			            $this->flash->error("Clve invalida intente nuevamente");
						$this->dispatcher->forward([
							'controller' => "login",
							'action' => 'index'
							]);

				return;
		
	             }


	        }else{
	        	$this->flash->error("Este usuario no se encuentra registrado");
				$this->dispatcher->forward([
					'controller' => "login",
					'action' => 'index'
					]);
			
			return;

			
	        } 


	       
	    }

    public function salirAction()
    {
        $this->session->remove('auth');
        return $this->response->redirect('/');

    }

}

