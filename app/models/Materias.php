<?php

class Materias extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=false)
     */
    public $nombre;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $profesor;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=false)
     */
    public $horario;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=false)
     */
    public $n_aula;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $created_at;


    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $estatus;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("db_univer_mipc");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'materias';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Usuarios[]|Usuarios
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Usuarios
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
