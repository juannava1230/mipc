<?php

$router = $di->getRouter();

//definicion de rutas de login y principales
///////////////////////////////////////////////
/////////////////////////////////////////////
$router->add(
    "/",
    [
        "controller" => "login",
        "action"     => "index",
    ]
);
$router->add(
    "/create",
    [
        "controller" => "login",
        "action"     => "create",
    ]
);
$router->add(
    "/entrar",
    [
        "controller" => "login",
        "action"     => "entrar",
    ]
);

$router->add(
    "/salir",
    [
        "controller" => "login",
        "action"     => "salir",
    ]
);

//definicion de rutas de Admin
///////////////////////////////////////////////
/////////////////////////////////////////////
$router->add(
    "/admin",
    [
        "controller" => "admin",
        "action"     => "index",
    ]
);
$router->add(
    "/crear-materias",
    [
        "controller" => "admin",
        "action"     => "index",
    ]
);
$router->add(
    "/estudiantes",
    [
        "controller" => "admin",
        "action"     => "estudiantes",
    ]
);







$router->handle();
