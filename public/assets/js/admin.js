$(document).ready( function () {
  $('.table-trans').DataTable({
    "paging": true,
    "info": true,
    "responsive": true,
    "aoColumnDefs": [{
        "bSortable": false,
        "aTargets": ["no-sort"]
    }],
    "bInfo": true,
         "language": {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}
  });

       $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });


        $( "#asignarMaterias" ).on( "submit", function( event ) {
        event.preventDefault();
        var resultado=$.ajax({
        url:"/admin/asignarMaterias", 
        data:$(this).serialize(),
        type:"POST",
        dataType:'text',
        async:false
        }).responseText;
        document.getElementById("result-consulta").innerHTML=resultado;
      
         });
     function Limpiar(){
         $("#asignarMaterias").empty();
         $('#materias').modal('hide');

    };



});
 function asignar(id) {
        var resultado=$.ajax({
        url:"/admin/AsignarMaterias", 
        data:{id:id},
        type:"POST",
        dataType:'text',
        async:false
        }).responseText;
        $("#container-Asignar-Materias").empty();
        document.getElementById("container-Asignar-Materias").innerHTML=resultado;
        $('#materias').modal('show');
};

 function verMaterias(id) {
        var resultado=$.ajax({
        url:"/admin/verMaterias", 
        data:{id:id},
        type:"POST",
        dataType:'text',
        async:false
        }).responseText;
        $("#container-Materias").empty();
        document.getElementById("container-Materias").innerHTML=resultado;
        $('#materiasAsignadas').modal('show');
};




$( "#asignarMaterias" ).on( "submit", function( event ) {
  event.preventDefault();
        var resultado=$.ajax({
        url:"/admin/asignarMateriasEstudiantes", 
        data:$(this).serialize(),
        type:"POST",
        dataType:'text',
        async:false
        }).responseText;
         document.getElementById("result-save").innerHTML=resultado;
        $('#materias').modal('hide');
        
    });



